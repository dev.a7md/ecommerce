const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const User = require('../models/userModel');
const signupValidation = require('../middlewares/signupValidation');
// const loginValidation = require('../middlewares/loginValidation');
const keys = require('../config');
const requireAuth = passport.authenticate('local', {session: false});
const jwtAuth = passport.authenticate('jwt', {session: false});

// test if user can access a specific page
router.get('/test', jwtAuth, async function (req, res) {
  // if there is a token will return this user
  res.send(req.user);
});

// POST user [registration]
router.post('/signup', signupValidation, async function (req, res) {
  const {name, email, password, membership, products} = req.body;

  // check if the user is exist
  const existingUser = await User.findOne({email});
  if (existingUser) return res.status(401).send('User already exist');

  // create and save a new user
  const user = new User({
    name,
    email,
    password,
    membership,
    products
  });
  const savedUser = await user.save();
  const token = jwt.sign({token: savedUser._id}, keys.jwtSecret);

  // send the token to the client
  res.send({token});
});

// signin with my custom validation
// router.post('/signin', loginValidation, async function (req, res) {
//   const {name, email} = req.user;
//   res.send({
//     name,
//     email
//   });
// });

// signin with passport local strategy
router.post('/signin', requireAuth, (req, res) => {
  // generate a new token after signing in...
  throw new Error('something went wrong');
  const token = jwt.sign({token: req.user._id}, keys.jwtSecret);
  res.send({token});
});

module.exports = router;
