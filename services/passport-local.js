const passport = require('passport');
const {Strategy} = require('passport-local');
const User = require('../models/userModel');
const bcrypt = require('bcrypt');

const options = {usernameField: 'email'};
const localStrategy = new Strategy(options, async function (email, password, done) {
  try {
    const user = await User.findOne({email});
    if (!user) return done(null, false, {message: 'Invalid data'});
    const result = await bcrypt.compare(password, user.password);
    if (!result) return done(null, false, {message: 'Invalid data'});
    done(null, user);
  } catch (err) {
    done(err)
  }
});
passport.use(localStrategy);
