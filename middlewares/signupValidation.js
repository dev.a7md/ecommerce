const Joi = require('joi');

module.exports = (req, res, next) => {
  const schema = {
    name: Joi.string().min(5).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(4).required(),
    membership: Joi.string(),
    products: Joi.array()
  };
  const {error} = Joi.validate(req.body, schema);
  if (error) {
    return res.status(401).send(error.details[0].message);
  }
  next()
};
