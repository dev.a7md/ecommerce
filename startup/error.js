const debug = require('debug')('app:error');
const {createLogger, transports, format} = require('winston');
const {combine, timestamp, json} = format;

const logger = createLogger({
  level: 'info',
  transports: [
    new transports.File({filename: 'logger.log'})
  ],
  format: combine(
    timestamp(),
    json()
  )
});

module.exports = (app) => {
  app.use(function (err, req, res, next) {
    logger.error(err.message);
    debug(err.message);
    res.status(500).send('Internal error');
  });
};
