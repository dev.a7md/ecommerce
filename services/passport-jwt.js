const passport = require('passport');
const {Strategy} = require('passport-jwt');
const {ExtractJwt} = require('passport-jwt');

const keys = require('../config');
const User = require('../models/userModel');

const opts = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: keys.jwtSecret
};
const jwtStrategy = new Strategy(opts, async function (payload, done) {
  try {
    const user = await User.findById(payload.token);
    console.log(user);
    if (!user) return done(null, false);
    done(null, user.email);
  } catch (err) {
    done(err.message);
  }
});
passport.use(jwtStrategy);
