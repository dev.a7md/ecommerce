module.exports = () => {
  require('../services/passport-local');
  require('../services/passport-jwt');
};
