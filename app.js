const express = require('express');
const app = express();

require('express-async-errors');

require('./startup/db')();
require('./startup/passport')();
require('./startup/engine')(app);
require('./startup/middlewares')(app);
require('./startup/routes')(app);
require('./startup/error')(app);

module.exports = app;
