const indexRouter = require('../routes/indexRouter');
const usersRouter = require('../routes/usersRouter');
const productRouter = require('../routes/productsRouter');

module.exports = function (app) {
  app.use('/', indexRouter);
  app.use('/api/users', usersRouter);
  app.use('/api/products', productRouter);
};
