const mongoose = require('mongoose');
const keys = require('../config');
const debug = require('debug')('app:db');

// options for deprecated warnings in mongoose
const opts = {
  useNewUrlParser: true,
  useCreateIndex: true
};

module.exports = () => {
  mongoose.connect(keys.mongoUri, opts)
    .then(debug('Connected to db'))
    .catch(err => debug(err));
};
