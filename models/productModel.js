const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'name is required']
  },
  price: {
    type: Number,
    required: [true, 'price is required'],
    min: 10
  },
  available: {
    type: Boolean,
    required: [true, 'available is required']
  },
  quantity: Number
});

module.exports = mongoose.model('product', productSchema);
