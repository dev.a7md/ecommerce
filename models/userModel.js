const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const debug = require('debug')('debug');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'name is required']
  },
  email: {
    type: String,
    required: [true, 'email is required'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'password is required']
  },
  membership: {
    type: String,
    default: 'Standard',
    enum: ['Standard', 'Premium', 'Enterprise']
  },
  products: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'product'
  }]
});

userSchema.pre('save', function (next) {
  const user = this;
  bcrypt.genSalt(10)
    .then((salt) => {
      bcrypt.hash(user.password, salt)
        .then(hash => {
          user.password = hash;
          next()
        })
        .catch(err => debug(`hashing error => ${err.message}`))
    })
    .catch(err => debug(`salt generation error => ${err.message}`));
  // mongoose.connection.collections['users'].drop();
});

module.exports = mongoose.model('user', userSchema);
