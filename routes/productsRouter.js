const express = require('express');
const router = express.Router();
const Product = require('../models/productModel');
const productValidate = require('../middlewares/productValidation');

// GET products listing
router.get('/', async function (req, res) {
  const products = await Product.find();
  res.send(products);
});

// POST product
router.post('/', productValidate, async function (req, res) {
  const {name, price, available, quantity} = req.body;
  try {
    const product = new Product({
      name,
      price,
      available,
      quantity
    });
    const savedUser = await product.save();
    res.send(savedUser);
  } catch (err) {
    res.send(err.message.split(':')[2].trim());
  }
});

module.exports = router;
