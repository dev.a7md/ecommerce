const bcrypt = require('bcrypt');
const User = require('../models/userModel');

module.exports = async (req, res, next) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});
  if (!user) {
    return res.status(401).send('Invalid email or password');
  }
  const result = await bcrypt.compare(password, user.password);
  if (!result) return res.status(401).send('Invalid email or password');
  req.user = user;
  next();
};
