const Joi = require('joi');

module.exports = (req, res, next) => {
  const schema = {
    name: Joi.string().min(5).required(),
    price: Joi.number().min(10).required(),
    available: Joi.boolean().required(),
    quantity: Joi.number()
  };
  const {error} = Joi.validate(req.body, schema);
  if (error) {
    return res.status(401).send(error.details[0].message);
  }
  next()
};
